package domain;

public class CalculatorParameters {

	private String rodzajUmowy;
	private String nettoCzyBrutto;
	private int rok;

	private double kwotaWynagrodzenia;
	private double kwotaWynagrodzeniaNetto;
	private double kwotaWynagrodzeniaBrutto;

	private float kosztUzyskaniaPrzychodu;

	private int skladkaRentowa;
	private int skladkaEmerytalna;
	private int skladkaChorobowa;
	private int skladkaZdrowotna;

	
	public String getRodzajUmowy() {
		return rodzajUmowy;
	}

	public void setRodzajUmowy(String rodzajUmowy) {
		this.rodzajUmowy = rodzajUmowy;
	}

	public String getNettoCzyBrutto() {
		return nettoCzyBrutto;
	}

	public void setNettoCzyBrutto(String nettoCzyBrutto) {
		this.nettoCzyBrutto = nettoCzyBrutto;
	}

	public int getRok() {
		return rok;
	}

	public void setRok(int rok) {
		this.rok = rok;
	}

	public double getKwotaWynagrodzenia() {
		return kwotaWynagrodzenia;
	}

	public void setKwotaWynagrodzenia(double kwotaWynagrodzenia) {
		this.kwotaWynagrodzenia = kwotaWynagrodzenia;
	}

	public double getKwotaWynagrodzeniaNetto() {
		return kwotaWynagrodzeniaNetto;
	}

	public void setKwotaWynagrodzeniaNetto(double kwotaWynagrodzeniaNetto) {
		this.kwotaWynagrodzeniaNetto = kwotaWynagrodzeniaNetto;
	}

	public double getKwotaWynagrodzeniaBrutto() {
		return kwotaWynagrodzeniaBrutto;
	}

	public void setKwotaWynagrodzeniaBrutto(double kwotaWynagrodzeniaBrutto) {
		this.kwotaWynagrodzeniaBrutto = kwotaWynagrodzeniaBrutto;
	}
	
	public float getKosztUzyskaniaPrzychodu() {
		return kosztUzyskaniaPrzychodu;
	}

	public void setKosztUzyskaniaPrzychodu(float kosztUzyskaniaPrzychodu) {
		this.kosztUzyskaniaPrzychodu = kosztUzyskaniaPrzychodu;
	}

	public int getSkladkaRentowa() {
		return skladkaRentowa;
	}

	public void setSkladkaRentowa(int skladkaRentowa) {
		this.skladkaRentowa = skladkaRentowa;
	}


	public int getSkladkaEmerytalna() {
		return skladkaEmerytalna;
	}

	public void setSkladkaEmerytalna(int skladkaEmerytalna) {
		this.skladkaEmerytalna = skladkaEmerytalna;
	}

	public int getSkladkaChorobowa() {
		return skladkaChorobowa;
	}

	public void setSkladkaChorobowa(int skladkaChorobowa) {
		this.skladkaChorobowa = skladkaChorobowa;
	}

	public int getSkladkaZdrowotna() {
		return skladkaZdrowotna;
	}

	public void setSkladkaZdrowotna(int skladkaZdrowotna) {
		this.skladkaZdrowotna = skladkaZdrowotna;
	}

}