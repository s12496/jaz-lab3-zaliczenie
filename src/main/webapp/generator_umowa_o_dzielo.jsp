<%@ page language="java" contentType="text/html; charset=UTF-8" 
    pageEncoding ="UTF-8"%>

<%@ page import="domain.*" %>
<%@ page import="services.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kalkulator płac v1.0 - Tabela dla Umowy o dzieło</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="services.CalculatorService" scope="application"/>

<jsp:setProperty name="parameters" property="rodzajUmowy" param="rodzajUmowy"/>
<jsp:setProperty name="parameters" property="rok" param="yrok"/>
<jsp:setProperty name="parameters" property="kwotaWynagrodzenia" param="kwotaWynagrodzenia"/>
<jsp:setProperty name="parameters" property="nettoCzyBrutto" param="nettoCzyBrutto"/>
<jsp:setProperty name="parameters" property="kosztUzyskaniaPrzychodu" param="kosztUzyskaniaPrzychodu"/>

<% 
String s = "brutto";
String p = parameters.getNettoCzyBrutto(); 
double kwotaWynagrodzenia = parameters.getKwotaWynagrodzenia();
double koszt = 0;
double podstawa = 0;
double zaliczka = 0;

if (s.equals(p)) { 
parameters.setKwotaWynagrodzeniaBrutto(kwotaWynagrodzenia);
koszt = parameters.getKwotaWynagrodzeniaBrutto() * (parameters.getKosztUzyskaniaPrzychodu() / 100);
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - koszt;
zaliczka = podstawa * 0.18;
parameters.setKwotaWynagrodzeniaNetto(parameters.getKwotaWynagrodzeniaBrutto() - zaliczka);
} else {
parameters.setKwotaWynagrodzeniaNetto(kwotaWynagrodzenia);
koszt = parameters.getKwotaWynagrodzeniaNetto() * 0.24;
parameters.setKwotaWynagrodzeniaBrutto(koszt * ((100 - parameters.getKosztUzyskaniaPrzychodu()) / parameters.getKosztUzyskaniaPrzychodu()));
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - parameters.getKosztUzyskaniaPrzychodu();
zaliczka = (parameters.getKwotaWynagrodzeniaBrutto() - parameters.getKosztUzyskaniaPrzychodu()) * 0.18;
}
%>

<h2>Tabela dla Umowy o dzieło:</h2>

<table border='1'>
<tr><th>Brutto</th><th>Koszt uzyskania przychodu</th><th>Podstawa opodatkowania</th><th>Zaliczka na PIT</th><th>Netto</th></tr>
<tr><td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaBrutto"/></td>
<td><%= Math.round(koszt * 100) / 100.0 %></td>
<td><%= Math.round(podstawa * 100) / 100.0 %></td>
<td><%= Math.round(zaliczka * 100) / 100.0 %></td>
<td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaNetto"/></td>

</table>

</body>
</html>