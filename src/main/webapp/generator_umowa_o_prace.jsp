<%@ page language="java" contentType="text/html; charset=UTF-8" 
    pageEncoding ="UTF-8"%>

<%@ page import="domain.*" %>
<%@ page import="services.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kalkulator płac v1.0 - Tabela dla Umowy o pracę</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="services.CalculatorService" scope="application"/>

<jsp:setProperty name="parameters" property="rodzajUmowy" param="rodzajUmowy"/>
<jsp:setProperty name="parameters" property="rok" param="rok"/>
<jsp:setProperty name="parameters" property="kwotaWynagrodzenia" param="kwotaWynagrodzenia"/>
<jsp:setProperty name="parameters" property="nettoCzyBrutto" param="nettoCzyBrutto"/>

<% 
String s = "brutto";
String p = parameters.getNettoCzyBrutto(); 
double kwotaWynagrodzenia = parameters.getKwotaWynagrodzenia();
double skladkaChorobowa = 0;
double skladkaEmerytalna = 0;
double skladkaRentowa = 0;
double skladkaZdrowotna = 0;
double podstawa = 0;
double zaliczka = 0;

if (s.equals(p)) { 
parameters.setKwotaWynagrodzeniaBrutto(kwotaWynagrodzenia);
skladkaChorobowa = parameters.getKwotaWynagrodzeniaBrutto() * (2.45 / 100);
skladkaEmerytalna = parameters.getKwotaWynagrodzeniaBrutto() * (9.76 / 100);
skladkaRentowa = parameters.getKwotaWynagrodzeniaBrutto() * (1.5 / 100);
skladkaZdrowotna = parameters.getKwotaWynagrodzeniaBrutto() * (7.766 / 100);
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - skladkaChorobowa - skladkaEmerytalna - skladkaRentowa - skladkaZdrowotna;
zaliczka = (parameters.getKwotaWynagrodzeniaBrutto() - podstawa) * 0.22;
parameters.setKwotaWynagrodzeniaNetto(podstawa - zaliczka);
} else {
parameters.setKwotaWynagrodzeniaNetto(kwotaWynagrodzenia);
parameters.setKwotaWynagrodzeniaBrutto(parameters.getKwotaWynagrodzeniaNetto() * 1.39);
skladkaChorobowa = parameters.getKwotaWynagrodzeniaBrutto() * (2.45 / 100);
skladkaEmerytalna = parameters.getKwotaWynagrodzeniaBrutto() * (9.76 / 100);
skladkaRentowa = parameters.getKwotaWynagrodzeniaBrutto() * (1.5 / 100);
skladkaZdrowotna = parameters.getKwotaWynagrodzeniaBrutto() * (7.75 / 100);
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - skladkaChorobowa - skladkaEmerytalna - skladkaRentowa - skladkaZdrowotna;
zaliczka = (parameters.getKwotaWynagrodzeniaBrutto() - podstawa) * 0.22;
} 
%>

<h2>Tabela dla Umowy o pracę:</h2>

<table border='1'>
<tr><th rowspan="2">M-c</th><th rowspan="2">Brutto</th><th colspan ="4">Ubezpieczenie</th><th rowspan="2">Podstawa opodatkowania</th><th rowspan="2">Zaliczka na PIT</th><th rowspan="2">Netto</th></tr>
<th>Emerytalne</th><th>Rentowe</th><th>Chorobowe</th><th>Zdrowotne</th></tr>
<% for(int i = 1; i < 13; i++ ) { %>
<tr><td><%=i%></td>
<td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaBrutto"/></td>
<td><%= Math.round(skladkaEmerytalna * 100) / 100.00 %></td>
<td><%= Math.round(skladkaRentowa * 100) / 100.00 %></td>
<td><%= Math.round(skladkaChorobowa * 100) / 100.00 %></td>
<td><%= Math.round(skladkaZdrowotna * 100) / 100.00 %></td>
<td><%= Math.round(podstawa * 100) / 100.00 %></td>
<td><%= Math.round(zaliczka * 100) / 100.00 %></td>
<td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaNetto"/></td>
</tr>
<% } %>

<tr><td>Suma</td>
<td><%= parameters.getKwotaWynagrodzeniaBrutto() * 12 %>
<td><%= Math.round(skladkaEmerytalna * 100 * 12) / 100.0 %></td>
<td><%= Math.round(skladkaRentowa * 12 * 100) /100.0 %></td>
<td><%= Math.round(skladkaChorobowa * 12 * 100) /100.0 %></td>
<td><%= Math.round(skladkaZdrowotna * 12 * 100) / 100.0 %></td>
<td><%= Math.round(podstawa * 12 * 100) /100.0 %></td>
<td><%= Math.round(zaliczka * 12 * 100) / 100.0 %></td>
<td><%= parameters.getKwotaWynagrodzeniaNetto() * 12 %>
</tr>
</table>

</body>
</html>