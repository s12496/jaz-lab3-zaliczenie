<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding ="UTF-8"%>

<%@ page import="domain.*" %>
<%@ page import="services.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Kalkulator płac v1.0 - Tabela dla Umowy Zlecenia</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="services.CalculatorService" scope="application"/>

<jsp:setProperty name="parameters" property="rodzajUmowy" param ="rodzajUmowy"/>
<jsp:setProperty name="parameters" property="rok" param ="rok"/>
<jsp:setProperty name="parameters" property="kwotaWynagrodzenia" param ="kwotaWynagrodzenia"/>
<jsp:setProperty name="parameters" property="nettoCzyBrutto" param ="nettoCzyBrutto"/>
<jsp:setProperty name="parameters" property="kosztUzyskaniaPrzychodu" param ="kosztUzyskaniaPrzychodu"/>
<jsp:setProperty name="parameters" property="skladkaRentowa" param ="skladkaRentowa"/>
<jsp:setProperty name="parameters" property="skladkaEmerytalna" param ="skladkaEmerytalna"/>
<jsp:setProperty name="parameters" property="skladkaChorobowa" param ="skladkaChorobowa"/>
<jsp:setProperty name="parameters" property="skladkaZdrowotna" param ="skladkaZdrowotna"/>

<% 
String s = "brutto";
String p = parameters.getNettoCzyBrutto(); 
double kwotaWynagrodzenia = parameters.getKwotaWynagrodzenia();
double skladkaChorobowa = 0;
double skladkaEmerytalna = 0;
double skladkaRentowa = 0;
double skladkaZdrowotna = 0;
double podstawa = 0;
double zaliczka = 0;
double koszt = 0;

if (s.equals(p)) { 
parameters.setKwotaWynagrodzeniaBrutto(kwotaWynagrodzenia);
koszt = parameters.getKwotaWynagrodzeniaBrutto() * (parameters.getKosztUzyskaniaPrzychodu() / 100);
skladkaChorobowa = parameters.getSkladkaChorobowa() * parameters.getKwotaWynagrodzeniaBrutto() * (2.45 / 100);
skladkaEmerytalna = parameters.getSkladkaEmerytalna() * parameters.getKwotaWynagrodzeniaBrutto() * (9.76 / 100);
skladkaRentowa = parameters.getSkladkaRentowa() * parameters.getKwotaWynagrodzeniaBrutto() * (1.5 / 100);
skladkaZdrowotna = parameters.getSkladkaZdrowotna() * parameters.getKwotaWynagrodzeniaBrutto() * (7.766 / 100);
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - skladkaChorobowa - skladkaEmerytalna - skladkaRentowa - skladkaZdrowotna;
zaliczka = (parameters.getKwotaWynagrodzeniaBrutto() - podstawa) * 0.22;
parameters.setKwotaWynagrodzeniaNetto(podstawa - zaliczka);
} else {
parameters.setKwotaWynagrodzeniaNetto(kwotaWynagrodzenia);
koszt = parameters.getKwotaWynagrodzeniaNetto() * 0.24;
parameters.setKwotaWynagrodzeniaBrutto(parameters.getKwotaWynagrodzeniaNetto() * 1.39);
skladkaChorobowa = parameters.getKwotaWynagrodzeniaBrutto() * (2.45 / 100);
skladkaEmerytalna = parameters.getKwotaWynagrodzeniaBrutto() * (9.76 / 100);
skladkaRentowa = parameters.getKwotaWynagrodzeniaBrutto() * (1.5 / 100);
skladkaZdrowotna = parameters.getKwotaWynagrodzeniaBrutto() * (7.766 / 100);
podstawa = parameters.getKwotaWynagrodzeniaBrutto() - skladkaChorobowa - skladkaEmerytalna - skladkaRentowa - skladkaZdrowotna;
zaliczka = (parameters.getKwotaWynagrodzeniaBrutto() - podstawa) * 0.22;
}
%>

<h2>Tabela dla Umowy Zlecenia:</h2>

<table border='1'>
<tr><th rowspan="2">Brutto</th><th colspan ="4">Ubezpieczenie</th><th rowspan="2">Koszt uzyskania przychodu</th><th rowspan="2">Podstawa opodatkowania</th><th rowspan="2">Zaliczka na PIT</th><th rowspan="2">Netto</th></tr>
<th>emerytalne</th><th>rentowe</th><th>chorobowe</th><th>zdrowotne</th></tr>

<td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaBrutto"/></td>
<td><%= Math.round(skladkaEmerytalna * 100) / 100.0 %></td>
<td><%= Math.round(skladkaRentowa * 100) / 100.0 %></td>
<td><%= Math.round(skladkaChorobowa * 100) / 100.0 %></td>
<td><%= Math.round(skladkaZdrowotna * 100) / 100.0 %></td>
<td><%= Math.round(koszt * 100) / 100.0 %></td>
<td><%= Math.round(podstawa * 100) / 100.0 %></td>
<td><%= Math.round(zaliczka * 100) / 100.0 %></td>
<td><jsp:getProperty name="parameters" property="kwotaWynagrodzeniaNetto"/></td>
</tr>
</table>

</body>
</html>