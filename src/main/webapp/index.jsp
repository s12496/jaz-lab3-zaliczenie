<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator płac v1.0 - Podstawowe dane</title>
</head>
<body>
<h2>Podstawowe dane</h2>

<form action="/sprawdz.jsp" method="post">

<h3>Rodzaj umowy:</h3>
<label><input type="radio" value="O Pracę" name="rodzajUmowy" id="rodzajUmowy" checked/>Umowa o pracę</label></br>
<label><input type="radio" value="O Dzieło" name="rodzajUmowy" id="rodzajUmowy"/>Umowa o dzieło</label></br>
<label><input type="radio" value="Zlecenie" name="rodzajUmowy" id="rodzajUmowy"/>Umowa zlecenie</label></br>

<h3>Rok:</h3>
<label><input type="number" id="rok" name="rok" value="2016"/></label></br>

<h3>Kwota wynagrodzenia:</h3>
<label><input type="number" id="kwotaWynagrodzenia" name="kwotaWynagrodzenia"/></label></br>

<h3>Rodzaj kwoty:</h3>
<label><input type="radio" value="netto" name="nettoCzyBrutto" id="nettoCzyBrutto" checked/>netto</label></br>
<label><input type="radio" value="brutto" name="nettoCzyBrutto" id="nettoCzyBrutto"/>brutto</label></br>
<br/>
<input type = "submit" value="Dalej"/>
</form>

</body>
</html>