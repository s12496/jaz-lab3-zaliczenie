<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="domain.*" %>
<%@ page import="javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator płac v1.0</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="services.CalculatorService" scope="application"/>

<jsp:setProperty name="parameters" property="rodzajUmowy" param="rodzajUmowy"/>
<jsp:setProperty name="parameters" property="rok" param="rok"/>
<jsp:setProperty name="parameters" property="kwotaWynagrodzenia" param="kwotaWynagrodzenia"/>
<jsp:setProperty name="parameters" property="nettoCzyBrutto" param="nettoCzyBrutto"/>

<% 
String uop = "O Pracę";
String uod = "O Dzieło";
String uz = "Zlecenie";
String rodzajUmowy = parameters.getRodzajUmowy();

if(uop.equals(rodzajUmowy)) {
response.sendRedirect("generator_umowa_o_prace.jsp");
} else if(uod.equals(rodzajUmowy)) { 
response.sendRedirect("umowa_o_dzielo.jsp");
} else if(uz.equals(rodzajUmowy)) {
response.sendRedirect("umowa_zlecenie.jsp");
}
%>

</body>
</html>