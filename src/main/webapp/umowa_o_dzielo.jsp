<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="domain.CalculatorParameters" %>
<%@ page import="javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Umowa o dzieło - dane szczegółowe</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="services.CalculatorService" scope="application"/>

<h2>Umowa o dzieło - dane szczegółowe</h2>

<form action="/generator_umowa_o_dzielo.jsp" method="post">
<label>Twój koszt przychodu to:</label></br>
<label><input type="radio" value="20" name="kosztUzyskaniaPrzychodu" id="kosztUzyskaniaPrzychodu" checked/>20%</label></br>
<label><input type="radio" value="50" name="kosztUzyskaniaPrzychodu" id="kosztUzyskaniaPrzychodu"/>50%</label></br><br/>

<input type = "submit" value="Dalej"/>
</form>

</body>
</html>