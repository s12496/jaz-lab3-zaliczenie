<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="domain.CalculatorParameters" %>
<%@ page import="javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Umowa zlecenie - Dane szczegółowe</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>

<h2>Umowa zlecenie - Dane szczegółowe</h2>

<form action="generator_umowa_zlecenie.jsp">
<label>Twój koszt przychodu to:</label></br>
<label><input type="radio" value="20" name="kosztUzyskaniaPrzychodu" id="kosztUzyskaniaPrzychodu"/ checked>20%</label></br>
<label><input type="radio" value="50" name="kosztUzyskaniaPrzychodu" id="kosztUzyskaniaPrzychodu"/>50%</label></br></br>

<label>Składka rentowa:</label></br>
<label><input type="radio" value="1" name="skladkaRentowa" id="skladkaRentowa" checked/>tak</label>
<label><input type="radio" value="0" name="skladkaRentowa" id="skladkaRentowa"/>nie</label></br>
<label>Składka emerytalna:</label></br>
<label><input type="radio" value="1" name="skladkaEmerytalna" id="skladkaEmerytalna" checked/>tak</label>
<label><input type="radio" value="0" name="skladkaEmerytalna" id="skladkaEmerytalna"/>nie</label></br>
<label>Składka chorobowa:</label></br>
<label><input type="radio" value="1" name="skladkaChorobowa" id="skladkaChorobowa" checked/>tak</label>
<label><input type="radio" value="0" name="skladkaChorobowa" id="skladkaChorobowa"/>nie</label></br>
<label>Składka zdrowotna:</label></br>
<label><input type="radio" value="1" name="skladkaZdrowotna" id="skladkaZdrowotna" checked/>tak</label>
<label><input type="radio" value="0" name="skladkaZdrowotna" id="skladkaZdrowotna"/>nie</label></br>
<br/>
<input type = "submit" value="Dalej"/>
</form>

</body>
</html>